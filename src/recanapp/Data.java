package recanapp;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author ryansalge
 */
public class Data {

    private final SimpleStringProperty Timestamp;
    private final SimpleStringProperty ID;
    private final SimpleStringProperty Value;

    public Data(String line) {
        String ts = "N/A";
        String id = "N/A";
        String value = "N/A";
        String[] items = line.split(",");
        if (items.length > 0) {
            ts = items[0];
            if (ts.startsWith("[")) {
                ts = ts.substring(1);
            }
        }
        if (items.length > 1) {
            id = items[1];
        }
        if (items.length > 2) {
            value = items[2];
            if (value.endsWith("]")) {
                value = value.substring(0, value.length() - 1);
            }
        }

        this.Timestamp = new SimpleStringProperty(ts);
        this.ID = new SimpleStringProperty(id);
        this.Value = new SimpleStringProperty(value);
    }

    public Data(String fName, String lName, String email) {
        this.Timestamp = new SimpleStringProperty(fName);
        this.ID = new SimpleStringProperty(lName);
        this.Value = new SimpleStringProperty(email);
    }

    public String getTimestamp() {
        return Timestamp.get();
    }

    public void setTimestamp(String fName) {
        Timestamp.set(fName);
    }

    public String getID() {
        return ID.get();
    }

    public void setID(String fName) {
        ID.set(fName);
    }

    public String getValue() {
        return Value.get();
    }

    public void setValue(String fName) {
        Value.set(fName);
    }

}
