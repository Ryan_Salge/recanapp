package recanapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Button;

/**
 *
 * @author ryansalge
 */
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import recanifx.ArduinoConnection;
import recanifx.ArduinoListener;

public class RecanApp extends Application implements ArduinoListener {

    private TableView<Data> table = new TableView<Data>();
    private final ObservableList<Data> data
            = FXCollections.observableArrayList();

    private ArduinoConnection arduinoConnection = new ArduinoConnection();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        final Label label = new Label("RECAN Data");
        label.setFont(new Font("Arial", 20));

        TableColumn TimestampCol = new TableColumn("Timestamp");
        TimestampCol.setMinWidth(100);
        TimestampCol.setCellValueFactory(
                new PropertyValueFactory<Data, String>("Timestamp"));

        TableColumn IDCol = new TableColumn("ID");
        IDCol.setMinWidth(100);
        IDCol.setCellValueFactory(
                new PropertyValueFactory<Data, String>("ID"));

        TableColumn ValueCol = new TableColumn("Value");
        ValueCol.setMinWidth(200);
        ValueCol.setCellValueFactory(
                new PropertyValueFactory<Data, String>("Value"));

        table.setEditable(false);
        table.setItems(data);
        table.getColumns().addAll(TimestampCol, IDCol, ValueCol);

        //create play button
        Button playButton = new Button();
        playButton.setText("Play");
        playButton.setOnAction(event -> {
            if (arduinoConnection.isConnected()) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Already Connected");
                alert.setHeaderText("You're already connected!");
                alert.setContentText("Stop before starting again!");

                alert.showAndWait();
                return;
            }
            final String[] portNames = arduinoConnection.getPortNames();
            if (portNames == null || portNames.length == 0) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("No Device Found");
                alert.setHeaderText("Nothing to connect!");
                alert.setContentText("Plug in your Arduino!");

                alert.showAndWait();
                return;
            }
            List<String> choices = new ArrayList<String>(Arrays.asList(portNames));
            ChoiceDialog<String> dialog = new ChoiceDialog<>(null, choices);
            dialog.setTitle("Choice Dialog");
            dialog.setHeaderText("Look, a Choice Dialog");
            dialog.setContentText("Choose your letter:");

            // Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                System.out.println("Your choice: " + result.get());
            }

// The Java 8 way to get the response value (with lambda expression).
            result.ifPresent(deviceName -> {
                System.out.println("Your choice: " + deviceName);
                arduinoConnection.connect(deviceName, this);
                    });
        });

        //create stop button
        Button stopButton = new Button();
        stopButton.setText("Stop");
        stopButton.setOnAction(event -> {
            arduinoConnection.disconnect();
        });

        //create file button
        Button fileButton = new Button();
        fileButton.setText("File");

        HBox hbox = new HBox();
        hbox.setSpacing(5);
        hbox.getChildren().addAll(playButton, stopButton, fileButton);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, hbox, table);

        Scene scene = new Scene(new Group());
        stage.setTitle("RECAN");
        stage.setWidth(800);
        stage.setHeight(800);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void onConnect(String portName) {
        System.out.println("Connected to '" + portName + "'");
    }

    @Override
    public void onMessage(String line) {
        Platform.runLater(() -> {
            System.out.println("Message '" + line + "'");
            data.add(new Data(line));
            if (data.size() > 100) {
                data.remove(0, data.size() - 100);
            }
        });
    }

    @Override
    public void onError(String message) {
        System.err.println("Error: '" + message + "'");
    }

    @Override
    public void onClose() {
        System.out.println("Disconnected");
    }

}
